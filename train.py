from extract_batch import extract_batch
from create_train_feature import feature_creation
from model import unet
import os
import shutil

feature_creation()
model = unet()

files = os.listdir("/content/analytics_4/train_dataset/wordgrid/")


for epoch in range(30):
  batch_chargrid, batch_seg = extract_batch(files)
  model.fit(x=batch_chargrid,y=batch_seg)

if os.path.exists('/content/analytics_4/unet_model'):
    shutil.rmtree('/content/analytics_4/unet_model')

model.save("/content/analytics_4/unet_model")
