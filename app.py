from flask_ngrok import run_with_ngrok
from flask import Flask,request, url_for, redirect, render_template
from segment import predict
import time

app = Flask(__name__)
run_with_ngrok(app)

@app.route("/", methods=["GET", "POST"])
def upload_image():

    if request.method == "POST":

        if request.files:
            image = request.files["image"]
            image.save("./analytics_4/static/uploaded_image." + image.filename.split('.')[-1])
            time.sleep(8)
            predict()

            return redirect(request.url + "static/predicted_image.jpg")

    return render_template("add_image_file.html")


@app.route("/predict", methods=["GET", "POST"])
def show_predict_image():
    return render_template("predict_file.html")

if __name__ == '__main__':
    app.run()
