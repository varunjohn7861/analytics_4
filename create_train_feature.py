import glob
from bs4 import BeautifulSoup as bs
import cv2
import pandas as pd
import numpy as np
from skimage.transform import resize
import os
import wikipedia2vec

n_height = 512
n_width = 512

def feature_creation():
    if not os.path.exists('/content/analytics_4/train_dataset/wordgrid'):
        os.makedirs('/content/analytics_4/train_dataset/wordgrid')

    if not os.path.exists('/content/analytics_4/train_dataset/gt'):
        os.makedirs('/content/analytics_4/train_dataset/gt')

    data_files = glob.glob("/content/analytics_4/dataset/*_ocr.xml")

    with open("enwiki_20180420_win10_100d.pkl.bz2","rb+") as file:
        embedding = wikipedia2vec.Wikipedia2Vec.load(file)

    word_vector = []
    for file in data_files:
        wordgrid_pd = pd.DataFrame(columns = ['left', 'top', 'width', 'height', 'word', 'conf'])
        with open(file.replace("\\","/")) as f:
            content = f.readlines()
            content = "".join(content)
            bs_content = bs(content,"lxml")

            file_name = bs_content.find("page").get("imagefilename")
            image = cv2.imread("/content/analytics_4/dataset/" + file_name)
            img_height = image.shape[0]
            img_width = image.shape[1]

            word_list = bs_content.find_all("word")
            for wordl in word_list:
                word_details = []
                points = wordl.find("coords").get("points").split(" ")
                dword = wordl.find("unicode").getText()
                conf = float(wordl.find("textequiv").get("conf"))
                
                x1,y1 = int(points[0].split(",")[0]), int(points[0].split(",")[1])
                x2,y2 = int(points[1].split(",")[0]), int(points[1].split(",")[1])
                x3,y3 = int(points[2].split(",")[0]), int(points[2].split(",")[1])
                x4,y4 = int(points[3].split(",")[0]), int(points[3].split(",")[1])


                xmin = min(x1,x2,x3,x4)
                xmax = max(x1,x2,x3,x4)
                ymin = min(y1, y2, y3, y4)
                ymax = max(y1, y2, y3, y4)

                width = xmax - xmin
                height = ymax - ymin

                wordgrid_pd = wordgrid_pd.append({
                        'left': xmin,
                        'top': ymin,
                        'width':width,
                        'height':height,
                        'word':dword,
                        'conf':conf
                        }, ignore_index = True)
                    
            wordgrid_pd['top'] = (wordgrid_pd['top']/img_height)*n_height
            wordgrid_pd['height'] = (wordgrid_pd['height']/img_height)*n_height
            wordgrid_pd['width'] = (wordgrid_pd['width']/img_width)*n_width
            wordgrid_pd['left'] = (wordgrid_pd['left']/img_width)*n_width

            wordgrid_pd['top'] = wordgrid_pd['top'].astype('int')
            wordgrid_pd['left'] = wordgrid_pd['left'].astype('int')
            wordgrid_pd['height'] = wordgrid_pd['height'].astype('int')
            wordgrid_pd['width'] = wordgrid_pd['width'].astype('int')

        wordgrid_gt = pd.DataFrame(columns = ['left', 'top', 'right', 'bot', 'class'])
        with open(file.replace("\\","/").replace("_ocr","_gt")) as f:
            content = f.readlines()
            content = "".join(content)
            bs_content = bs(content,"lxml")

            properties = bs_content.find_all("textregion")

            for x in properties:
                if x.find("property"):
                    if x.find("property").get("value") == "supplier":
                        points = x.find("coords").get("points").split(" ")
                        x1,y1 = float(points[0].split(",")[0]), float(points[0].split(",")[1])
                        x2, y2 = float(points[1].split(",")[0]), float(points[1].split(",")[1])
                        x3, y3 = float(points[2].split(",")[0]), float(points[2].split(",")[1])
                        x4, y4 = float(points[3].split(",")[0]), float(points[3].split(",")[1])


                        xmin = int(float(min(x1,x2,x3,x4)))
                        xmax = int(float(max(x1,x2,x3,x4)))
                        ymin = int(float(min(y1, y2, y3, y4)))
                        ymax = int(float(max(y1, y2, y3, y4)))

                        wordgrid_gt = wordgrid_gt.append({
                                        'left': xmin,
                                        'top': ymin,
                                        'right':xmax,
                                        'bot':ymax,
                                        'class': 1
                                        }, ignore_index = True)



                    elif x.find("property").get("value") == "receiver":
                        points = x.find("coords").get("points").split(" ")
                        x1, y1 = float(points[0].split(",")[0]), float(points[0].split(",")[1])
                        x2, y2 = float(points[1].split(",")[0]), float(points[1].split(",")[1])
                        x3, y3 = float(points[2].split(",")[0]), float(points[2].split(",")[1])
                        x4, y4 = float(points[3].split(",")[0]), float(points[3].split(",")[1])

                        xmin = int(float(min(x1,x2,x3,x4)))
                        xmax = int(float(max(x1,x2,x3,x4)))
                        ymin = int(float(min(y1, y2, y3, y4)))
                        ymax = int(float(max(y1, y2, y3, y4)))

                        wordgrid_gt = wordgrid_gt.append({
                                        'left': xmin,
                                        'top': ymin,
                                        'right':xmax,
                                        'bot':ymax,
                                        'class': 2
                                        }, ignore_index = True)

        wordgrid_gt['top'] = (wordgrid_gt['top']/img_height)*n_height
        wordgrid_gt['bot'] = (wordgrid_gt['bot']/img_height)*n_height
        wordgrid_gt['right'] = (wordgrid_gt['right']/img_width)*n_width
        wordgrid_gt['left'] = (wordgrid_gt['left']/img_width)*n_width
            
        wordgrid_gt['top'] = wordgrid_gt['top'].astype('int')
        wordgrid_gt['left'] = wordgrid_gt['left'].astype('int')
        wordgrid_gt['right'] = wordgrid_gt['right'].astype('int')
        wordgrid_gt['bot'] = wordgrid_gt['bot'].astype('int')

        for index, row in wordgrid_pd.iterrows():
            try:
                wordgrid_pd.loc[index,'class'] = wordgrid_gt.loc[(wordgrid_gt['top']<=row['top']) & (wordgrid_gt['left']<=row['left']) & (wordgrid_gt['right']>=(row['left']+row['width'])) & (wordgrid_gt['bot']>=(row['top']+row['height']))]['class'].values[0]
            except:
                wordgrid_pd.loc[index,'class'] = 0

        wordgrid_pd.sort_values(by="conf", ascending=True, inplace=True)
        wordgrid_pd.reset_index(drop=True, inplace=True)


        embedding_array_2 = np.zeros([n_height,n_width,100])
        embedding_array_gt = np.zeros([n_height,n_width], dtype=int)

        for index,row in wordgrid_pd.iterrows():
                    try:
                        word = row.word.lower()
                        word2 = ''.join(e for e in word if e.isalnum())
                        embedding_array_2[row.left:(row.left+row.width),row.top:(row.top+row.height)] = embedding.get_word_vector(word2) 
                        if row['class']>0:
                            embedding_array_gt[row.left:(row.left+row.width),row.top:(row.top+row.height)] = row['class']
                    except:
                        pass

        embedding_array_gt = np.eye(3)[embedding_array_gt]

        try:
            np.save("/content/analytics_4/train_dataset/wordgrid/" + file_name.split(".")[0] + ".npy",embedding_array_2)
            np.save("/content/analytics_4/train_dataset/gt/" + file_name.split(".")[0] + ".npy",embedding_array_gt) 
        except:
            pass