# Analytics_4

### This project is created for Semantic Segmentation of Invoice Documents.

To run the project run **[Application_run_notebook](https://colab.research.google.com/drive/1ScblMIknQ14K4ihGNWkwgY5w6bhpPvuP?usp=sharing)** Colab notebook. The notebook will create and open a webiste on which user can upload images of the invoice and in return will get a segmented image of invoice where red colored segments show sender and receiver.

Contains codes used by colab notebooks:\
    1. [Application_run_notebook](https://colab.research.google.com/drive/1ScblMIknQ14K4ihGNWkwgY5w6bhpPvuP?usp=sharing) \
    2. [model_creation_notebook](https://colab.research.google.com/drive/1jl0gX5NECxT5tfFG__lykIiOMe7TJjuR?usp=sharing)

Also contains `.ipynb` files of these notebooks

Files and Folder Description: \
    1. [dataset](./dataset/) - Contains original training dataset created by [1]. \
    2. [static](./sattic) - Contains `.css` stylesheet files along with images used by the `html` page. \
    3. [templates](./templates) - Contains `.html` used by flask application for the pages. \
    4. [unet_model](./unet_model) - Contains trained UNet model used by the flasj application. \
    5. [app.py](./app.py) - Code of the flask application. \
    6. [train.py](./train.py) - code to train and save the UNet model.

#### References: \
[1] - [Riba, P., Dutta, A., Goldmann, L., Forn´es, A., Ramos, O., & Llad´os, J., (2019) Table detection in invoice documents by graph neural networks.](https://www.researchgate.net/publication/339028094_Table_Detection_in_Invoice_Documents_by_Graph_Neural_Networks)