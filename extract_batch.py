import numpy as np
pochs = 10
batch_size = 8
prop_test = 0.2
seed = 123456
pad_left_range = 0.2
pad_top_range = 0.2
pad_right_range = 0.2
pad_bot_range = 0.2
width = 512
height = 512
nb_anchors = 3

def extract_batch(dataset, batch_size=6): # Data augmentation
    if batch_size > len(dataset):
        raise NameError('batch_size > len(dataset)')
    np.random.shuffle(dataset)
    
    tab_rand = np.random.rand(batch_size, 4)*[pad_left_range*width, pad_top_range*height, pad_right_range*width, pad_bot_range*height]
    tab_rand = tab_rand.astype(int)
    
    chargrid_input = []
    seg_gt = []
    anchor_mask_gt = []
    anchor_coord_gt = []
    
    for i in range(0, batch_size):
        data = np.load("/content/analytics_4/train_dataset/wordgrid/"+ dataset[i])
        chargrid_input.append(data)
        
        data = np.load("/content/analytics_4/train_dataset/gt/"+ dataset[i])
        seg_gt.append(data)
    
    return np.array(chargrid_input), np.array(seg_gt)